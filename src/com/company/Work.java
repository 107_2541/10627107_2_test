package com.company;

import java.util.Scanner;

public class Work {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int a = input.nextInt();
        if (a % 2 == 0)
            System.out.println(a + "是偶數");
        else
            System.out.println(a + "是奇數");
    }
}